package com.example.p9l2

import android.content.Context
import android.content.SharedPreferences

class MPWidgetPref(context: Context) {
    private val sharePrefFileName = "sharePrefIndex"
    private val indexKey = "KeyRand"
    private val maxKey = "maxSong"
    private val isplaying = "playingStatus"
    private var myPreferences : SharedPreferences

    init{
        myPreferences  = context!!.getSharedPreferences(sharePrefFileName,Context.MODE_PRIVATE)
    }

    var songIndex: Int
        get() = myPreferences.getInt(indexKey,0)

        set(value) {
            if(value<0)
                myPreferences.edit().putInt(indexKey,0).commit()
            else if(value>maxSongIndex)
                myPreferences.edit().putInt(indexKey,maxSongIndex).commit()
            else
                myPreferences.edit().putInt(indexKey,value).commit()
        }

    var maxSongIndex: Int
        get() = myPreferences.getInt(maxKey,0)
        set(value) {
            myPreferences.edit().putInt(maxKey,value-1).commit()

            if(songIndex > maxSongIndex){
                songIndex = maxSongIndex-1
            }
        }

    var isSongPlaying : Boolean
        get() = myPreferences.getBoolean(isplaying, false)
        set(value) {
            myPreferences.edit().putBoolean(isplaying, value).commit()
        }
}