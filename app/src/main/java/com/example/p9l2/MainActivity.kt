package com.example.p9l2

import android.Manifest
import android.annotation.SuppressLint
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.mpwidget.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.ArrayList
import java.util.concurrent.Future




class MainActivity : AppCompatActivity() {
    private var async: Future<Unit>? = null
    private lateinit var songProvider: SongProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkUserPermission()
    }

    private fun setProgressBar(reset: Boolean, play: Boolean) {
        if (reset) {
            progressBar.progress = 0
        }

        if (play) {
            async = doAsync {
                uiThread {
                    progressBar.max = songProvider.sharedSongList[songProvider.mpWidgetPref!!.songIndex].duration!!.toInt()
                }
                while (progressBar.progress <= progressBar.max) {
                    progressBar.progress += 1000
                    Thread.sleep(1000)
                }
            }
        }
        else {
            if(async!=null)
                async!!.cancel(true)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initMedia() {
        updateSongUI()

        play_media.setOnClickListener {
            songProvider.playSong()
            songProvider.mpWidgetPref!!.isSongPlaying = true
            setProgressBar(true, true)
        }

        stop_media.setOnClickListener {
            songProvider.stopSong()
            songProvider.mpWidgetPref!!.isSongPlaying = false
            setProgressBar(true, false)
        }

        forward_media.setOnClickListener {
            songProvider.forwardSong()
            progressBar.progress += 2000
        }

        rewind_media.setOnClickListener {
            songProvider.rewindSong()
            progressBar.progress -= 2000
        }

        next_media.setOnClickListener {
            songProvider.nextSong()

            updateSongUI()
            if(async!=null)
                async!!.cancel(true)
            setProgressBar(true, songProvider.mpWidgetPref!!.isSongPlaying)
        }

        previous_media.setOnClickListener {
            songProvider.previousSong()
            updateSongUI()
            if(async!=null)
                async!!.cancel(true)
            setProgressBar(true, songProvider.mpWidgetPref!!.isSongPlaying)
        }
    }

    private fun checkUserPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 123)
            }else{
                takeOverSongProvider()
                initMedia()
            }
        }
    }

    private fun convertDuration(duration: Long): String? {
        var out: String? = null
        val hours: Long
        try {
            hours = duration / 3600000
        } catch (e: Exception) {
            e.printStackTrace()
            return out
        }

        val remainingMinutes = (duration - hours * 3600000) / 60000
        var minutes = remainingMinutes.toString()
        if (minutes == "0") {
            minutes = "00"
        }
        val remainingSeconds = duration - hours * 3600000 - remainingMinutes * 60000
        var seconds = remainingSeconds.toString()
        seconds = if (seconds.length < 2) {
            "00"
        } else {
            seconds.substring(0, 2)
        }

        out = if (hours > 0) {
            hours.toString() + ":" + minutes + ":" + seconds
        } else {
            "$minutes:$seconds"
        }

        return out
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            123 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //do Nothing
            } else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                checkUserPermission()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }

    }

    private fun updateSongUI(){
        namaLagu.text = "${songProvider.sharedSongList[songProvider.mpWidgetPref!!.songIndex].songName}} ${songProvider.sharedSongList[songProvider.mpWidgetPref!!.songIndex].artistName}"
        durasi.text = convertDuration(songProvider.sharedSongList[songProvider.mpWidgetPref!!.songIndex].duration!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopService(songProvider.myIntent)

        songProvider.mpWidgetPref!!.isSongPlaying = false
    }

    override fun onResume() {
        super.onResume()

        updateSongUI()
        songProvider.loadSong()
    }

    override fun onStop() {
        super.onStop()
        val appWidgetManager = AppWidgetManager.getInstance(this)
        val ids = appWidgetManager.getAppWidgetIds(ComponentName(this, MPWidget::class.java))
        val updateIntent = Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE)
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
        sendBroadcast(updateIntent)
    }

    private fun takeOverSongProvider(){
        songProvider = SongProvider
        songProvider.init(applicationContext)

        songProvider.loadSong()
    }
}