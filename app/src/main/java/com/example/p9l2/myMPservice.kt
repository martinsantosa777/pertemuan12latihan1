package com.example.p9l2

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder

const val ACTION_PLAY = "PLAY"
const val ACTION_PAUSE = "PAUSE"
const val ACTION_STOP = "STOP"
const val ACTION_FORWARD = "FORWARD"
const val ACTION_REWIND = "REWIND"
const val ACTION_CREATE = "CREATE"

class MyMediaPlayerService : Service(),
    MediaPlayer.OnPreparedListener,
    MediaPlayer.OnErrorListener,
    MediaPlayer.OnCompletionListener {

    private var myMediaPlayer: MediaPlayer? = null
    var isPlayed = false
    private fun init(url: String) {
        myMediaPlayer = MediaPlayer()
        myMediaPlayer!!.setDataSource(url)
        myMediaPlayer!!.setOnPreparedListener(this)
        myMediaPlayer!!.setOnCompletionListener(this)
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            val actionIntent = intent.action
            when (actionIntent) {
                ACTION_CREATE -> init(intent.getStringExtra("song"))
                ACTION_PLAY -> {
                    if (!myMediaPlayer!!.isPlaying && !isPlayed) {
                        myMediaPlayer!!.prepareAsync()
                        isPlayed = true
                    } else {
                        myMediaPlayer!!.start()
                    }
                }
                ACTION_PAUSE -> {
                    if (isPlayed)
                        myMediaPlayer!!.pause()
                }
                ACTION_STOP -> {
                    if (myMediaPlayer!!.isPlaying) {
                        myMediaPlayer!!.stop()
                        isPlayed = false
                    }
                }
                ACTION_FORWARD -> {
                    if (myMediaPlayer!!.isPlaying) {
                        myMediaPlayer!!.seekTo(myMediaPlayer!!.currentPosition + 2000)
                    }
                }
                ACTION_REWIND -> {
                    if (myMediaPlayer!!.isPlaying) {
                        myMediaPlayer!!.seekTo(myMediaPlayer!!.currentPosition - 2000)
                    }
                }
            }
        }
        return flags
    }

    override fun onError(p0: MediaPlayer?, p1: Int, p2: Int): Boolean {
        return false
    }

    override fun onCompletion(p0: MediaPlayer?) {

    }

    override fun onPrepared(p0: MediaPlayer?) {
        myMediaPlayer!!.start()
    }

    override fun onDestroy() {
        if (myMediaPlayer != null) {
            myMediaPlayer!!.stop()
            myMediaPlayer!!.release()
        }
    }
}