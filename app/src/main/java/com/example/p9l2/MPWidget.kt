package com.example.p9l2

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import kotlinx.android.synthetic.main.mpwidget.view.*
import java.util.ArrayList

/**
 * Implementation of App Widget functionality.
 */
class MPWidget : AppWidgetProvider() {

    private var myIntent: Intent? = null

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
    }

    override fun onDisabled(context: Context) {
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        myIntent = Intent(context, MyMediaPlayerService::class.java)
        if (PreviousOnClick.equals(intent.getAction())) {
            songProvider.previousSong()
        } else if (NextOnClick.equals(intent.getAction())) {
            songProvider.nextSong()
        } else if (PlayOnClick.equals(intent.action)) {
            songProvider.playSong()
            songProvider.mpWidgetPref!!.isSongPlaying = true
        } else if (PauseOnClick.equals(intent.action)) {
            songProvider.pauseSong()
            songProvider.mpWidgetPref!!.isSongPlaying = false
        } else if (StopOnClick.equals(intent.action)) {
            songProvider.stopSong()
            songProvider.mpWidgetPref!!.isSongPlaying = false
        } else if (RewindOnClick.equals(intent.action)) {
            songProvider.rewindSong()
        } else if (ForwardOnClick.equals(intent.action)) {
            songProvider.forwardSong()
        }

        val appWidgetManager = AppWidgetManager.getInstance(context)
        val thisAppWidgetComponentName = ComponentName(context.getPackageName(), javaClass.getName())
        val appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidgetComponentName)
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    companion object {
        private val PlayOnClick = "PlayOnClick"
        private val PauseOnClick = "PauseOnClick"
        private val StopOnClick = "StopOnClick"
        private val PreviousOnClick = "PreviousOnClick"
        private val NextOnClick = "NextOnClick"
        private val RewindOnClick = "RewindOnClick"
        private val ForwardOnClick = "ForwardOnClick"
        private lateinit var songProvider: SongProvider

        internal fun updateAppWidget(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int) {
            val views = RemoteViews(context.packageName, R.layout.mpwidget)
            songProvider = SongProvider
            songProvider.init(context)

            views.setTextViewText(
                R.id.appwidget_text_size,
                "Song Index : ${songProvider.mpWidgetPref!!.songIndex.toString()}"
            )
            views.setTextViewText(
                R.id.appwidget_song_title,
                "${songProvider.sharedSongList[songProvider.mpWidgetPref!!.songIndex].songName} ${songProvider.sharedSongList[songProvider.mpWidgetPref!!.songIndex].artistName}"
            )
            views.setOnClickPendingIntent(R.id.appwidget_btn_previous, getPendingSelfIntent(context, PreviousOnClick))
            views.setOnClickPendingIntent(R.id.appwidget_btn_next, getPendingSelfIntent(context, NextOnClick))
            views.setOnClickPendingIntent(R.id.appwidget_btn_play, getPendingSelfIntent(context, PlayOnClick))
            views.setOnClickPendingIntent(R.id.appwidget_btn_pause, getPendingSelfIntent(context, PauseOnClick))
            views.setOnClickPendingIntent(R.id.appwidget_btn_stop, getPendingSelfIntent(context, StopOnClick))
            views.setOnClickPendingIntent(R.id.appwidget_btn_rewind, getPendingSelfIntent(context, RewindOnClick))
            views.setOnClickPendingIntent(R.id.appwidget_btn_forward, getPendingSelfIntent(context, ForwardOnClick))

            appWidgetManager.updateAppWidget(appWidgetId, views)
        }

        protected fun getPendingSelfIntent(context: Context, buttonStr: String): PendingIntent {
            val intent = Intent(context, MPWidget::class.java)
            intent.action = buttonStr
            return PendingIntent.getBroadcast(context, 0, intent, 0)
        }

    }

}

