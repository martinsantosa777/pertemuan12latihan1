package com.example.p9l2

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.provider.MediaStore

import java.util.ArrayList


@SuppressLint("StaticFieldLeak")
object SongProvider {
    var myIntent: Intent? = null
    val sharedSongList = ArrayList<SongInfo>()
    var context: Context? = null
    var mpWidgetPref: MPWidgetPref? = null

    fun init(thecontenxt: Context) {
        context = thecontenxt
        if (sharedSongList.isEmpty()) {
            getSongs()
        }
        if (myIntent == null) {
            myIntent = Intent(context, MyMediaPlayerService::class.java)
        }
        if (mpWidgetPref == null) {
            mpWidgetPref = MPWidgetPref(thecontenxt)
            mpWidgetPref!!.maxSongIndex = sharedSongList.size
        }
    }

    private fun getSongs() {
        val uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val selection = MediaStore.Audio.Media.IS_MUSIC + "!=0"
        val cursor = context!!.contentResolver.query(uri, null, selection, null, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    val name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
                    val artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
                    val duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION))
                    val url = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA))

                    val s = SongInfo(name, artist, url, duration)
                    sharedSongList.add(s)

                } while (cursor.moveToNext())
            }
            cursor.close()
        }
    }

    fun loadSong() {
        myIntent!!.action = ACTION_CREATE
        myIntent!!.putExtra("song", sharedSongList[mpWidgetPref!!.songIndex].songUrl)
        context!!.startService(myIntent)
    }

    fun playSong() {
        //tidak perlu loadSong()

        myIntent!!.action = ACTION_PLAY
        context!!.startService(myIntent)
    }

    fun pauseSong() {
        //tidak perlu loadSong()

        myIntent!!.action = ACTION_PAUSE
        context!!.startService(myIntent)
    }

    fun stopSong() {
        //tibak boleh mpWidgetPref!!.isSongPlaying = false disini
        //karena kalau skenarionya:
        //1.Lagu diplay
        //2.Tekan next
        //3. isPlaying jadi false --> Padahal setelah di next harus mengikuti status isPlaying sebelumnya
        myIntent!!.action = ACTION_STOP
        context!!.startService(myIntent)

    }

    fun nextSong() {
        if ((mpWidgetPref!!.songIndex + 1) < sharedSongList.size - 1) {
            var tmp = mpWidgetPref!!.songIndex + 1
            mpWidgetPref!!.songIndex = tmp
            var a = 2
            changeSong()
        }
    }

    fun previousSong() {
        if ((mpWidgetPref!!.songIndex - 1) > 0) {
            mpWidgetPref!!.songIndex -= 1
            changeSong()
        }
    }

    private fun changeSong() {
        stopSong() //matikan lagu sebelumnya
        loadSong()
        //kalau lagu sebelumnya udh sedang diplay, ketika next maka auto play
        //tidak boleh pakai loadSong() karena loadSong mengecek status play sembelumnya, jika sebelumnya sedang play maka tidak boleh diplay lagi
        //padahal kita mau timpa playernya
        myIntent!!.action = ACTION_PLAY
        context!!.startService(myIntent)
    }

    fun rewindSong() {
        myIntent!!.action = ACTION_REWIND
        context!!.startService(myIntent)
    }

    fun forwardSong() {
        myIntent!!.action = ACTION_FORWARD
        context!!.startService(myIntent)
    }
}